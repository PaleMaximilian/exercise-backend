import {PostgresConnectionOptions} from "typeorm/driver/postgres/PostgresConnectionOptions";

let port: number;

try {
      port = parseInt(process.env.PORT);
} catch {
      port = undefined;
}

const config: PostgresConnectionOptions = {
      type: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      port: port || 5432,
      database: process.env.DB || 'postgres',
      username: process.env.POSTGRES_USER || 'test',
      password: process.env.POSTGRES_PASS || 'test',
      entities: ['dist/src/**/*.entity.js'],
      synchronize: true,
    };

export default config;
