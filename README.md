## Installation

```bash
$ npm install
```
## Set-up
```bash
# environment variables and their defaults
POSTGRES_HOST=localhost
POSTGRES_USER=test
POSTGRES_PASS=test
DB=postgres
PORT=5432

# AUTH (has to be set)
API_KEY=1ab2c3d4e5f61ab2c3d4e5f6

# running postgres with docker
$ docker run --env-file .env -p 5432:5432 postgres
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Auth

- Set a header 'X-API-KEY'

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger

`localhost:3000/docs`
