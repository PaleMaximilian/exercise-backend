import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { Job } from './entities/job.entity';
import { JobsController } from './jobs.controller';
import { JobsService } from './jobs.service';

describe('JobsController', () => {
  let jobsController: JobsController;
  let mockJobsService = {
    findAll: jest.fn().mockResolvedValue(true),
    findOne: jest.fn(),
    create: jest.fn().mockImplementation((dto) => Promise.resolve(dto)),
    update: jest.fn(),
    remove: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobsController],
      providers: [JobsService],
    })
      .overrideProvider(JobsService)
      .useValue(mockJobsService)
      .compile();

    jobsController = module.get<JobsController>(JobsController);
  });

  it('should be defined', () => {
    expect(jobsController).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of jobs', async () => {
      let response = await jobsController.findAll();
      expect(response).toBe(true);
    });
  });

  describe('findOne', () => {
    describe('if the id exists', () => {
      it('should return an item with that id', async () => {
        mockJobsService.findOne.mockImplementation((id) =>
          Promise.resolve({ id, result: true }),
        );
        let id = 1;
        let response = await jobsController.findOne(id);
        expect(response).toStrictEqual({ id: 1, result: true });
      });
    });
    describe('if the id does not exist', () => {
      it('should throw a NotFoundException', async () => {
        mockJobsService.findOne.mockImplementation((_) => {
          throw new NotFoundException();
        });
        let id = 1;
        try {
          await jobsController.findOne(id);
        } catch (e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobsService.findOne).toBeCalledWith(id);
        }
      });
    });
  });
  describe('create', () => {
    it('should return the object passed/created', async () => {
      let dto = new CreateJobDto();
      dto.title = 'job';
      dto.location = 'some place';
      dto.description = 'test';
      dto.hourly_pay = 20;
      let result = await jobsController.create(dto);
      expect(result).toBe(dto);
      expect(mockJobsService.create).toBeCalledWith(dto);
    });
  });
  describe('update', () => {
    let id = 1;
    let dto = new UpdateJobDto();
    dto.title = 'job';
    dto.location = 'some place';
    dto.description = 'test';
    dto.hourly_pay = 20;

    describe('if job exists', () => {
      it('should return the updated object', async () => {
        mockJobsService.update.mockImplementation((_, dto) => dto);
        let result = await jobsController.update(id, dto);
        expect(result).toBe(dto);
        expect(mockJobsService.update).toBeCalledWith(id, dto);
      });
    });

    describe('if job does not exist', () => {
      it('should throw a NotFoundException', async () => {
        mockJobsService.update.mockImplementation((_id, _dto) => {
          throw new NotFoundException();
        });
        try {
          await jobsController.update(id, dto);
        } catch (e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobsService.update).toBeCalledWith(id, dto);
        }
      });
    });
  });
  describe('delete', () => {
    describe('if the id exists', () => {
      it('should return the deleted job', async () => {
        mockJobsService.remove.mockImplementation((id) => {
          return Promise.resolve({id, result: true});
        });
        let id = 1;
        let result = await jobsController.remove(id);
        expect(result).toStrictEqual({id: 1, result: true});
        expect(mockJobsService.remove).toBeCalledWith(id);
      });
    });
    describe('if the id does not exist', () => {
      it('should throw a NotFoundException', async () => {
        mockJobsService.remove.mockImplementation( _id => {
          throw new NotFoundException();
        });
        let id = 1;
        try {
          await jobsController.remove(id);
        } catch(e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobsService.remove).toBeCalledWith(id);
        }
      });
    });
  });
});
