import {NotFoundException} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {getRepositoryToken} from '@nestjs/typeorm';
import {CreateJobDto} from './dto/create-job.dto';
import {UpdateJobDto} from './dto/update-job.dto';
import {Job} from './entities/job.entity';
import { JobsService } from './jobs.service';

describe('JobsService', () => {
  let service: JobsService;
  let jobs: Array<Job> = [];
  let mockJobRepository = {
    save: jest.fn().mockImplementation((new_job: Job): Promise<Job> => {
      let old_job = jobs.find(job => job.id === new_job.id);
      if (!old_job){
        new_job.created_at = new Date();
        new_job.updated_at = new Date();
        jobs.push(new_job);
        return Promise.resolve(new_job);
      }
      old_job.updated_at = new Date();
      old_job.title = new_job.title;
      old_job.description = new_job.description;
      old_job.location = new_job.location;
      old_job.hourly_pay = new_job.hourly_pay;
      return Promise.resolve(new_job);
    }),
    create: jest.fn().mockImplementation((dto: CreateJobDto): Job => {
      let job = new Job();
      Object.assign(job, dto);
      job.id = jobs.length + 1;
      return job;
    }),
    findOneOrFail: jest.fn().mockImplementation((id: number): Job => {
      let found = jobs.find(job => job.id === id);
      if (!found) {
        throw new NotFoundException();
      }
      return found;
    }),
    find: jest.fn().mockImplementation((): Array<Job> => jobs ),
    remove: jest.fn().mockImplementation((jobToRemove: Job): Promise<Job> => {
      let toRemove = jobs.find(job => job.id === jobToRemove.id);
      jobs = jobs.filter(job => job.id !== jobToRemove.id);
      return Promise.resolve(toRemove);
    })
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JobsService,
        {
          provide: getRepositoryToken(Job),
          useValue: mockJobRepository
        }
      ],
    }).compile();

    service = module.get<JobsService>(JobsService);
    jobs = []
    let baseDto: CreateJobDto = {
      title: 'Job 1',
      description: 'Test',
      location: 'Someplace',
      hourly_pay: 20
    };
    let job = mockJobRepository.create(baseDto);
    mockJobRepository.save(job);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of jobs', async () => {
      let result = service.findAll();
      expect(result).toStrictEqual(jobs);
    });
  });
  describe('findOne', () => {
    describe('if job exists', () => {
      it('should return that job', async () => {
        let result = await service.findOne(1)
        expect(result).toStrictEqual(jobs[0]);
        expect(mockJobRepository.findOneOrFail).toBeCalledWith(1);
      });
    });
    describe('if job does not exist', () => {
      it('should throw a NotFoundException', async () => {
        try {
          await service.findOne(2);
        } catch(e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobRepository.findOneOrFail).toBeCalledWith(2);
        }
      });
    });
  });
  describe('create', () => {
    it('should create and return a job', async () => {
      let dto = new CreateJobDto();
      dto.title = 'Job 2';
      dto.description = 'Test';
      dto.location = 'Someplace';
      dto.hourly_pay = 5;
      let result = await service.create(dto);
      expect(result.id).toStrictEqual(2);
      expect(result.updated_at).toBeDefined();
      expect(jobs.length).toStrictEqual(2);
    });
  });
  describe('update', () => {
    let dto = new UpdateJobDto();
    dto.title = 'New title';
    dto.location = 'New place';
    describe('if job exists', () => {
      it('should update and return that job', async () => {
        let result = await service.update(1, dto);
        expect(result.id).toEqual(1);
        expect(result.title).toStrictEqual('New title');
        expect(result.location).toStrictEqual('New place');
        expect(result.hourly_pay).toStrictEqual(20);
        expect(jobs.length).toStrictEqual(1);
        expect(mockJobRepository.findOneOrFail).toBeCalledWith(1);
      });
    });
    describe('if job does not exist', () => {
      it('should throw a NotFoundException', async () => {
        try {
          await service.findOne(2);
        } catch(e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobRepository.findOneOrFail).toBeCalledWith(2);
        }
      });
    });
  });
  describe('remove', () => {
    describe('if job exists', () => {
      it('should remove and return that job', async () => {
        let toRemove = jobs[0];
        let result = await service.remove(1);
        expect(result).toStrictEqual(toRemove);
        expect(jobs.length).toStrictEqual(0);
        expect(mockJobRepository.findOneOrFail).toBeCalledWith(1);
      });
    });
    describe('if job does not exist', () => {
      it('should throw a NotFoundException', async () => {
        try {
          await service.remove(2);
        } catch(e) {
          expect(e).toStrictEqual(new NotFoundException());
          expect(mockJobRepository.findOneOrFail).toBeCalledWith(2);
        }
      });
    });
  });
});
