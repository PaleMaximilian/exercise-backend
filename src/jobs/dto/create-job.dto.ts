import {ApiProperty} from "@nestjs/swagger";
import {IsAlphanumeric, IsNotEmpty, IsPositive} from "class-validator";

export class CreateJobDto {

  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  location: string;

  @ApiProperty()
  @IsPositive()
  hourly_pay: number;
}
