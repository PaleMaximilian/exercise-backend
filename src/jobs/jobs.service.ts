import { Injectable, NotFoundException } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import {Job} from './entities/job.entity';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(Job) private jobsRepository: Repository<Job>
  ) {}
  create(createJobDto: CreateJobDto) {
    const job = this.jobsRepository.create(createJobDto);
    return this.jobsRepository.save(job);
  }

  findAll() {
    return this.jobsRepository.find();
  }

  async findOne(id: number) {
    try {
      const job = await this.jobsRepository.findOneOrFail(id);
      return job;
    } catch {
      throw new NotFoundException;
    }
  }
  async update(id: number, updateJobDto: UpdateJobDto) {
    const originaljob = await this.findOne(id);
    const updatedjob = Object.assign(originaljob, updateJobDto);

    return this.jobsRepository.save(updatedjob)

  }

  async remove(id: number) {
    const job = await this.findOne(id);
    return this.jobsRepository.remove(job);
  }
}
