import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, BadRequestException, UseGuards } from '@nestjs/common';
import { JobsService } from './jobs.service';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import {ApiBadRequestResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {Job} from './entities/job.entity';
import {AuthGuard} from '@nestjs/passport';

@ApiTags('jobs')
@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @ApiCreatedResponse({type: Job})
  @ApiBadRequestResponse()
  @UseGuards(AuthGuard('api-key'))
  @Post()
  create(@Body() createJobDto: CreateJobDto) {
    return this.jobsService.create(createJobDto);
  }

  @ApiOkResponse({type: Job, isArray: true})
  @UseGuards(AuthGuard('api-key'))
  @Get()
  findAll() {
    return this.jobsService.findAll();
  }

  @ApiOkResponse({type: Job})
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  @UseGuards(AuthGuard('api-key'))
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.jobsService.findOne(id);
  }

  @ApiOkResponse({type: Job})
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  @UseGuards(AuthGuard('api-key'))
  @Patch(':id')
  update(@Param('id', ParseIntPipe) id: number, @Body() updateJobDto: UpdateJobDto) {
    return this.jobsService.update(id, updateJobDto);
  }

  @ApiOkResponse({type: Job})
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  @UseGuards(AuthGuard('api-key'))
  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.jobsService.remove(id);
  }
}
