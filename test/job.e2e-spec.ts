import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobsModule } from '../src/jobs/jobs.module';
import {Repository} from 'typeorm';
import { Job } from '../src/jobs/entities/job.entity';
import { CreateJobDto } from '../src/jobs/dto/create-job.dto';
import {ConfigModule} from '@nestjs/config';
import {AuthModule} from '../src/auth/auth.module';

describe('Job Module (e2e)', () =>{
  let app: INestApplication;
  let repository: Repository<Job>

  beforeAll(async () => {
    const module = await Test.createTestingModule(
      {
        imports: [
          JobsModule,
          TypeOrmModule.forRoot({
            type: 'sqlite',
            database: ':memory:',
            entities: [Job],
            synchronize: true
          }),
          ConfigModule.forRoot(),
          AuthModule
        ]
      }
    ).compile();
    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    repository = module.get('JobRepository');
  });

  beforeEach(async () => {
    const jobs: Job[] = [
      {id: 1, updated_at: new Date(), created_at: new Date(), title: 'Test job', description: 'Testing', location: 'Someplace', hourly_pay: 20},
      {id: 2, updated_at: new Date(), created_at: new Date(), title: 'Test job 2', description: 'Testing 2', location: 'Someplace', hourly_pay: 30},
    ]
    for (let job of jobs){
      await repository.save(job);
    }
  });

  afterEach(async () => {
    await repository.query('DELETE FROM job;');
  });

  afterAll(async () => {
    await app.close();
  });

  describe('AUTH', () => {
    describe('When API key is not provided or not valid', () => {
      it('should return 401', async () => {
        await request.agent(app.getHttpServer())
          .get('/jobs')
          .expect('Content-Type', /json/)
          .expect(401);
      });
    });
  });

  describe('GET /jobs', () => {
    it('should return an array of jobs', async () => {
      const { body } = await request.agent(app.getHttpServer())
        .get('/jobs')
        .set('Accept', 'application/json')
        .set('X-API-KEY', process.env.API_KEY)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(body).toEqual([
        {id: expect.any(Number), title: 'Test job', description: 'Testing', location: 'Someplace', hourly_pay: 20, created_at: expect.any(String), updated_at: expect.any(String)},
        {id: expect.any(Number), title: 'Test job 2', description: 'Testing 2', location: 'Someplace', hourly_pay: 30, created_at: expect.any(String), updated_at: expect.any(String)},
      ]);
    });
  });
  describe('GET /jobs/:id', () => {
    describe('if the id exists', () => {
      it('should return that job', async () => {
        const { body } = await request.agent(app.getHttpServer())
          .get('/jobs/1')
          .set('Accept', 'application/json')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(200)

        expect(body).toEqual(
          {id: 1, title: 'Test job', description: 'Testing', location: 'Someplace', hourly_pay: 20, created_at: expect.any(String), updated_at: expect.any(String)},
        );
      });
    });
    describe('if the id does not exist', () => {
      it('should return 404', async () => {
        await request.agent(app.getHttpServer())
          .get('/jobs/3')
          .set('Accept', 'application/json')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(404)
      });
    });
    describe('if the id is not a number', () => {
      it('should return 400', async () => {
        await request.agent(app.getHttpServer())
          .get('/jobs/abc')
          .set('Accept', 'application/json')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(400)
      });
    });
  });
  describe('POST /jobs', () => {
    describe('if the request body is valid', () => {
      it('should create and return a new job', async () => {
        const { body } = await request.agent(app.getHttpServer())
          .post('/jobs/')
          .send({
            title: 'Job 3',
            description: 'Testing',
            location: 'Someplace',
            hourly_pay: 20,
          })
          .expect('Content-Type', /json/)
          .set('X-API-KEY', process.env.API_KEY)
          .expect(201);

        expect(body).toEqual(
          {id: expect.any(Number), title: 'Job 3', description: 'Testing', location: 'Someplace', hourly_pay: 20, created_at: expect.any(String), updated_at: expect.any(String)}
        );
      });
    });
    describe('if the request body is not valid', () => {
      it('should return 400', async () => {
        await request.agent(app.getHttpServer())
          .post('/jobs/')
          .send({
            description: 'Testing',
            location: 'Someplace',
            hourly_pay: 'abc',
          })
          .set('X-API-KEY', process.env.API_KEY)
          .expect(400)
      });
    });
  });
  describe('PATCH /jobs/:id', () => {
    describe('if the id exists and the body is valid', () => {
      it('should update and return the job', async () => {
        const { body } = await request.agent(app.getHttpServer())
          .patch('/jobs/1')
          .send({
            title: 'New Job'
          })
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual(
          {id: expect.any(Number), title: 'New Job', description: 'Testing', location: 'Someplace', hourly_pay: 20, created_at: expect.any(String), updated_at: expect.any(String)}
        );
        expect(body.created_at).not.toEqual(body.updated_at);
      });
    });
    describe('if the id does not exist', () => {
      it('should return 404', async () => {
        await request.agent(app.getHttpServer())
          .patch('/jobs/4')
          .send({
            title: 'New Job'
          })
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(404);
      });
    });
    describe('if the request body is invalid', () => {
      it('should return 400', async () => {
        await request.agent(app.getHttpServer())
          .patch('/jobs/1')
          .send({
            title: ''
          })
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(400);
      });
    });
  });
  describe('DELETE /jobs/:id', () => {
    describe('if the id exists', () => {
      it('should remove and return the removed job', async () => {
        const { body } = await request.agent(app.getHttpServer())
          .delete('/jobs/1')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual(
          {updated_at: expect.any(String), created_at: expect.any(String), title: 'Test job', description: 'Testing', location: 'Someplace', hourly_pay: 20},
        );
      });
    });
    describe('if the id does not exist', () => {
      it('should return 404', async () => {
        await request.agent(app.getHttpServer())
          .delete('/jobs/3')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(404);
      });
    });
    describe('if the id is invalid', () => {
      it('should return 400', async () => {
        await request.agent(app.getHttpServer())
          .delete('/jobs/abc')
          .set('X-API-KEY', process.env.API_KEY)
          .expect('Content-Type', /json/)
          .expect(400);
      });
    });
  });
});
