alter table jobs column id -- Remove primary_key from id

ALTER TABLE jobs add column uuid(varchar) -- new primary_key uuid

uuid_generate()

alter table jobs column uuid(primary_key)

-- v1 -> v2 : v2 uses uuid and v1 uses id(numerical)
-- create still generates numerical id for some time
-- at some point start doing deprecation warnings
